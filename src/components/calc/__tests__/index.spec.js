import { shallowMount } from '@vue/test-utils';
import CalcFraction from '../index.vue';

describe('Component Calc', () => {
  it('2/3 + 2/3 = 3', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            1.5,
          ],
          operators: ['+'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(3);
  });

  it('2/3 - 2/3 = 0', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            1.5,
          ],
          operators: ['-'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(0);
  });

  it('2/3 * 2/3 = 2.25', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            1.5,
          ],
          operators: ['*'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(2.25);
  });

  it('2/3 / 2/3 = 1', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            1.5,
          ],
          operators: ['/'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(1);
  });

  it('2 + 2 * 2 = 6', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            2,
            2,
            2,
          ],
          operators: ['+', '*'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(6);
  });

  it('2/3 * -2/3 = -2.25', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            -1.5,
          ],
          operators: ['*'],
        },
      },
    });

    expect(wrapper.vm.result.valueOf()).toBe(-2.25);
  });

  it('При добавление дроби добавляется и оператор', () => {
    const wrapper = shallowMount(CalcFraction, {
      propsData: {
        value: {
          fractions: [
            1.5,
            1.5,
          ],
          operators: ['+'],
        },
      },
    });

    wrapper.vm.addFraction();

    expect(wrapper.vm.fractions.length).toBe(3);
    expect(wrapper.vm.operators.length).toBe(2);
  });
});
