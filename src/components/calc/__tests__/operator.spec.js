import { mount } from '@vue/test-utils';
import CalcOperator from '../operator.vue';

describe('Component CalcOperator', () => {
  it('есть дефолтное значение', () => {
    const wrapper = mount(CalcOperator);

    const { value } = wrapper.vm.$options.props;

    expect(value.default).toBeDefined();
    expect(wrapper.vm.value).toBeDefined();
  });

  it('смена значение на "-"', () => {
    const wrapper = mount(CalcOperator);

    const select = wrapper.find('select');
    const options = select.findAll('option');

    options.at(1).setSelected();
    select.trigger('input');

    expect(wrapper.emitted().input).toBeTruthy();
  });

  it('валидатор', () => {
    const wrapper = mount(CalcOperator);

    const { value } = wrapper.vm.$options.props;

    expect(value.type).toBe(String);

    expect(value.validator && value.validator('+')).toBeTruthy();
    expect(value.validator && value.validator('-')).toBeTruthy();
    expect(value.validator && value.validator('*')).toBeTruthy();
    expect(value.validator && value.validator('/')).toBeTruthy();

    expect(value.validator && value.validator('a')).toBeFalsy();
  });
});
