import Fraction from 'fraction.js';
import { mount } from '@vue/test-utils';
import CalcFraction from '../fraction.vue';

describe('Component CalcFraction', () => {
  it('инициализация', () => {
    const wrapper = mount(CalcFraction, {
      propsData: {
        value: new Fraction(1.5),
      },
    });

    expect(wrapper.vm.n).toBe(3);
    expect(wrapper.vm.d).toBe(2);
  });

  it('вызов события при корректном значение', () => {
    const wrapper = mount(CalcFraction, {
      propsData: {
        value: new Fraction(1.5),
      },
    });

    const inputs = wrapper.findAll('.calc-fraction__input');

    inputs.at(1).setValue(4);

    expect(wrapper.emitted().input).toBeTruthy();
  });

  it('не вызов события при некорректном значение,', () => {
    const wrapper = mount(CalcFraction, {
      propsData: {
        value: new Fraction(1.5),
      },
    });

    const inputs = wrapper.findAll('.calc-fraction__input');

    inputs.at(1).setValue(0);

    expect(wrapper.emitted().input).toBeFalsy();
  });
});
