import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/calc',
      name: 'calc',
      component: () => import(/* webpackChunkName: "calc" */ './views/Calc.vue'),
    },
    {
      path: '/socket',
      name: 'socket',
      component: () => import(/* webpackChunkName: "socket" */ './views/Socket.vue'),
    },
  ],
});
